package com.example.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by THINKPAD on 2018/9/16.
 * 基于类型安全自动配置
 */
@Component
@ConfigurationProperties(prefix = "author")
//@PropertySource(value="/application.properties") //location属性取消了
public class Author{
    private String name;
    private String age;

    public Author() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
