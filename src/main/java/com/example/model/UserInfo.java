package com.example.model;




import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Administrator on 2018/10/28.
 */
@Entity
public class UserInfo {
    @Id
    public String userId;
    public String userName;
    public String passWord;
    public String tel;
    public String email;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserInfo(String userId, String userName, String passWord, String tel, String email) {
        this.userId = userId;
        this.userName = userName;
        this.passWord = passWord;
        this.tel = tel;
        this.email = email;
    }

    public UserInfo() {
    }
}
