package com.example.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * Created by Administrator on 2018/10/5.
 * 这是正向工程的，不是逆向工程
 */
@Entity //声明这是一个和数据库表映射的实体类
@NamedQuery(name="Student.withNameAndAddressNamedQuery",
        query="select s from Student s where s.name=?1 and s.address=?2")
public class Student {
    @Id //指明是主键
    @GeneratedValue //使用默认自增，hibernate 会默认为我们自动生成一个名叫  HIBERNATE_SEQUENCE 的序列
    private Long id;
    private String name;
    private Integer age;
    private String address;

    public Student() {
    }

    public Student(Long id, String name, Integer age, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
