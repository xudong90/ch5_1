package com.example.controller;

import com.example.model.Person;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/10/4.
 * 返回两个，一个对象和一个集合model
 */
@SpringBootApplication
@Controller
public class IndexController {
    @RequestMapping("/index")
    public String index(Model model){
        Person singlePerson = new Person("张三","14");
        Person person1 = new Person("antina","15");
        Person person2 = new Person("李西","19");
        Person person3 = new Person("张三","14");

        List<Person> list = new ArrayList<Person>();
        list.add(person1);
        list.add(person2);
        list.add(person3);

        model.addAttribute("singlePerson",singlePerson);
        model.addAttribute("personList",list);
        return "index";
    }
}
