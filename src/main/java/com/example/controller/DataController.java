package com.example.controller;

import com.example.dao.StudentRepository;
import com.example.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Administrator on 2018/10/5.
 */
@RestController
public class DataController {
    @Autowired //spring jpa已经自动注册bean，所以可以直接注入
     StudentRepository studentRepository;

    /**
     * 保存
     * save 支持批量保存：<S extends T> Iterable<S> save(Iterable<S> entities);
     * 删除
     * 支持使用id删除对象，批量删除以及删除全部
     * void delete(ID id);
     * void delete(T entity);
     * void delete(Iterable<? extends T> entities);
     * void deleteAll();
     * @param name
     * @param address
     * @param age
     * @return
     */
    @RequestMapping("/save")
    public Student save(String name,String address,Integer age){
        Student s = studentRepository.save(new Student(null,name,age,address));
        return s;
    }

    /**
     * 测试findByAddress
     * @param address
     * @return
     */
    @RequestMapping("/q1")
    public List<Student> q1(String address){
        List<Student> student = studentRepository.findByAddress(address);
        return student;
    }

    /**
     * 测试findByNameAndAddress
     * @param name
     * @param address
     * @return
     */
    @RequestMapping("/q2")
    public Student q2(String name,String address){
        Student student = studentRepository.findByNameAndAddress(name,address);
        return student;
    }

    /**
     * 测试使用：@Query绑定查询
     * @param name
     * @param address
     * @return
     */
    @RequestMapping("/q3")
    public Student q3(String name,String address){
        Student student = studentRepository.withNameAndAddressQuery(name,address);
        return student;
    }

    /**
     * 测试 namedQuery查询
     * @param name
     * @param address
     * @return
     */
    @RequestMapping("/q4")
    public Student q4(String name,String address){
        Student student = studentRepository.withNameAndAddressNamedQuery(name,address);
        return student;
    }

    /**
     * 测试排序
     * @return
     */
    @RequestMapping("/sort")
    public List<Student> sort(){
        List<Student>students = studentRepository.findAll(new Sort(Sort.Direction.DESC,"age"));
        return students;
    }

    /**
     * 测试分页
     * @return
     * 暂时不能用，有错误
     */
    @RequestMapping("/page")
    public Page<Student>page(){
        Page<Student>studentPage = studentRepository.findAll(PageRequest.of(0,1));
        System.out.print("分页数据 studentPage is : "+studentPage);
        return studentPage;
    }
}
