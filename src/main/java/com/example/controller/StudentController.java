package com.example.controller;

import com.example.dao.StudentRepository;
import com.example.model.Student;
import org.codehaus.plexus.logging.LoggerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Administrator on 2018/10/28.
 */
@SpringBootApplication
@Controller
@RequestMapping("/stu")
public class StudentController {
    @Autowired
    StudentRepository studentRepository;
    Logger logger = Logger.getLogger("logger");
    @RequestMapping("/student")
    public String getStuList(Model model){
        List<Student> stuList = studentRepository.findAll(new Sort(Sort.Direction.ASC,"id"));
        System.out.print(stuList);
        model.addAttribute("stuList",stuList);
        return "student";
    }
}
