package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class Ch51Application {

	public static void main(String[] args) {
		SpringApplication.run(Ch51Application.class, args);
	}
}
