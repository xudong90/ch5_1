package com.example.dao;


import com.example.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

/**
 * Created by Administrator on 2018/10/5.
 * 定义数据访问接口
 */
public interface StudentRepository extends JpaRepository<Student,Long> {

    //使用方法名查询：接受一个name参数，返回列表
    List<Student> findByAddress(String address);

    //使用方法名查询：
    Student findByNameAndAddress(String name,String address);

    //按照名称绑定，使用query查询
    @Query("select s from Student s where s.name=:name and s.address=:address")
    Student withNameAndAddressQuery(@Param("name")String name,@Param("address")String address);

    //namedQuery查询，在实体类已经定义了
    Student withNameAndAddressNamedQuery(String name,String address);


}
