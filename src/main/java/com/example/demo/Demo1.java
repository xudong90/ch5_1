package com.example.demo;

import com.example.model.Author;
import com.example.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THINKPAD on 2018/9/16.
 */
@SpringBootApplication
@Controller
@ResponseBody
@RequestMapping("/demo1")
public class Demo1 {
    @Value("${book.author}")
    private String author;
    @Value("${book.name}")
    private String name;
    @Autowired(required = false)
    private Author authors;
    @RequestMapping("/getMsg")
    public String getMsg() {
        return "getMsg : 第一个springboot 项目";
    }
    @RequestMapping("/getName")
    public String gerName(){
        return "book name is "+name+"="+"book author is "+author;
    }
    @RequestMapping("/getAuthor")
    public String getAuthor(){
        return "author name is "+authors.getName()+"作者年龄是"+authors.getAge();
    }

}
